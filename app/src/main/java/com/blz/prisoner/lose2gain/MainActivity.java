package com.blz.prisoner.lose2gain;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    ViewPager quick_viewpager;
    QuickNotificationViewPagerAdapter adapter;
    TabLayout tab_indicator;

    LinearLayout home_first_linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /* set up viewPager for quickNotification **/
        quick_viewpager = findViewById(R.id.quick_viewpager);

        tab_indicator = findViewById(R.id.tab_indicator);

        LinearLayout notification_section = findViewById(R.id.linear4);
        ScrollView dashboard_scrollview = findViewById(R.id.dashboard_scrollview);
        dashboard_scrollview.setVerticalScrollBarEnabled(false);
        //dashboard_scrollview.setHorizontalScrollBarEnabled(false);



        //layout button for add client
        home_first_linear = findViewById(R.id.home_first_linear);



        List<QuickNotificationInfo> infoList = new ArrayList<>();

        infoList.add(new QuickNotificationInfo("101","Bishal", "29/08/2018"));
        infoList.add(new QuickNotificationInfo("202","Akash", "25/03/2017"));
        infoList.add(new QuickNotificationInfo("303","XYZ", "29/02/2018"));
        infoList.add(new QuickNotificationInfo("404","Sady", "29/02/2018"));
        infoList.add(new QuickNotificationInfo("303","Abir", "29/02/2018"));

        adapter = new QuickNotificationViewPagerAdapter(MainActivity.this,infoList);
        quick_viewpager.setAdapter(adapter);

        /*Set up tab indicator with view pager*/
        tab_indicator.setupWithViewPager(quick_viewpager);

        Animation fade_in,slide_in;
        //YoYo.with(Techniques.DropOut).duration(3000).playOn(notification_section);

        fade_in = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);

        notification_section.setAnimation(fade_in);


        slide_in = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_bottom);
        dashboard_scrollview.setAnimation(slide_in);


        home_first_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AddClient.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });


    }
}
